var path = require('path');
var webpack = require('webpack');

module.exports = {
    context: path.resolve('js'),
    entry: './index.js',
    output: {
        path: path.resolve('build/'),
        publicPath: '/public/assets/',
        filename: 'bundle.js'
    },

    devServer: {
        contentBase: 'public'
    },

    watch: false,

    plugins: [
        new webpack.BannerPlugin('*******************\nGenerated by webpack\n*******************\n')
    ],

    module: {
            preLoaders: [{
                test: /\.js$/,
                exclude: /node_modules|bower_components/,
                loader: 'jshint-loader'
            }],
            loaders: [
                {
                    test: /\.js$/,
                    exclude: /node_modules|bower_components/,
                    loader: 'babel-loader',
                    query: {
                     presets: ['es2015']
                    }
                },
                {
                    test: /\.(png|jpg|ttf|eot)/,
                    exclude: /node_modules|bower_components/,
                    loader: 'url-loader?limit=10000'
                }
             ]
     }
};
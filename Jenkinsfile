def majorVersionNumber = 1.0
def version = "${majorVersionNumber}.${env.BUILD_NUMBER}"

node {

    try {

        stage 'Checkout'

        // checkout code
        git 'https://bitbucket.org/renewinkler84/skitour-contest.git'

        // setup Java 8
        env.JAVA_HOME = tool 'Java 8'
        env.PATH = "${env.JAVA_HOME}/bin:${env.PATH}"

        // setup maven
        def mvnHome = tool 'Maven 3'
        env.PATH = "${mvnHome}/bin:${env.PATH}"

        stage 'Build' // compile, test and package

        // change directory
        dir('skitourcontest-frontend') {
            // pull dependencies from npm
            sh 'npm install'
        }

        // change directory
        dir('skitourcontest-parent') {
            // set the version of the build artifact to the Jenkins BUILD_NUMBER so you can map artifacts to Jenkins builds
            sh "mvn versions:set -DnewVersion=${version}"
            // compile and package
            sh 'mvn clean install'
        }

        // stash code & dependencies to expedite subsequent testing
        // and ensure same code & dependencies are used throughout the pipeline
        // stash is a temporary archive
        stash name: 'everything',
                excludes: 'test-results/**',
                includes: '**'

    } catch (err) {
        echo "Caught: ${err}"
        notify("Error ${err}")
        currentBuild.result = 'FAILURE'
    }

    stage 'Archival'

    try {

        // JUnit Test results
        step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])

        // archive jar file
        archiveArtifacts artifacts: 'skitourcontest-application/target/*.jar', excludes: null

    } catch (err) {
        echo "Caught: ${err}"
        notify("Error ${err}")
        currentBuild.result = 'FAILURE'
    }

}


stage('Acceptance') {

// parallel integration testing
    parallel chrome: {
        runTests("Chrome")
    }, firefox: {
        runTests("Firefox")
    }, ie: {
        runTests("IE")
    }, phantomJs: {
        runTests("PhantomJs")
    }
}

def runTests(browser) {
    node {
        // clear workspace
        sh 'rm -rf *'

        unstash 'everything'

        // change directory
        dir('skitourcontest-frontend') {
            sh "npm run test-single-run -- --browsers ${browser}"

            step([$class: 'JUnitResultArchiver',
                  testResults: 'test-results/**/test-results.xml'])
        }

    }
}

stage name: 'Staging', concurrency: 1

// deploy to a docker container mapped to port 3000
// on windows use: bat 'docker-compose up -d --build'
sh 'docker-compose up -d --build'

stage name: 'Capacity', concurrency: 1

node {
    echo "no yet implemented"
}

stage name: 'Manual Tests', concurrency: 1

def parallelStages = [:]
parallelStages["UX Tests"] = {
    node {
        // deploy to staging
        def warFiles = findFiles glob: '**/target/*.war'
        // deploy(warFiles[0].path)

        notifyTesters('UX test')
    }
    // wait for test feedback
    timeout(time: 5, unit: 'DAYS') {
        input 'UX Test successful?'
    }
}
parallelStages["Security Tests"] = {
    node {
        notifyTesters('security test')
    }
    input 'Security Test successful?'
}
parallel parallelStages

stage name: 'Production', concurrency: 1

// input statement always outside out node. Otherwise it blocks the node
// and takes this ressource away from other builds.
input 'Deploy to Production?'

node {

// https://github.com/g0t4/solitaire-systemjs-course/blob/jenkins2-course-jf/Jenkinsfile

    // deploy to a docker container mapped to port 3000
    // on windows use: bat 'docker-compose up -d --build'
    sh 'docker-compose up -d --build'

    emailext(to: 'renewinkler@gmx.ch',
            subject: "Successful release to production",
            body: """<p>New release <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${
                env.BUILD_NUMBER
            }]</a> was successfully deployed to production.</p>""",
    )

}

def notifyTesters(typeOfTests) {
    emailext(
            to: "renewinkler@gmx.ch",
            subject: "WAITING for ${typeOfTests}",
            body: """<p>Job <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a> is waiting for ${
                typeOfTests
            }.</p>
        <p>Please follow the link and approve or deny the next step.</p>""",
    )
}

def notify(status) {
    emailext(
            to: "renewinkler@gmx.ch",
            subject: "${status}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
            body: """<p>${status}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
        <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""",
    )
}
package ch.skitourcontest;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Servlet initializer is used, when a war file is created that is deployed (e.g. to AWS).
 */
public class SkitourContestServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SkitourContestApplication.class);
    }
}

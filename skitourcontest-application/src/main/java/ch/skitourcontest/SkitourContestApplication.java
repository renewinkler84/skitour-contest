package ch.skitourcontest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class SkitourContestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkitourContestApplication.class, args);
    }

    @RestController
    public class HelloController {

        @RequestMapping("/hello")
        public String sayHello() {
            return "Hello Skitour Contest!";
        }
    }
}
